<?php

namespace App\Controller;

use App\Event\WeatherStateEvent;
use App\Model\WeatherStation;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\Response;

class DefaultController
{
    /**
     * @return Response
     *
     * @throws \Exception
     */
    public function index(WeatherStation $weatherStation, EventDispatcherInterface $eventDispatcher)
    {
        $weatherStation->setTemperature(10);
        $event = new WeatherStateEvent($weatherStation);
        $eventDispatcher->dispatch($event, WeatherStateEvent::NAME);
        dump($weatherStation->getTemperature());
        return new Response("ok");
    }

    public function sendEmail(WeatherStation $weatherStation, EventDispatcherInterface $eventDispatcher)
    {
        $weatherStation->setTemperature(10);

        $event = new WeatherStateEvent($weatherStation);
        $eventDispatcher->dispatch($event, WeatherStateEvent::NAME);
        $eventPression = new WeatherStateEvent($weatherStation);
        $eventDispatcher->dispatch($eventPression, WeatherStateEvent::PRESSION);
        return new Response($weatherStation->getTemperature()."");
    }
}