<?php


namespace App\Event;

use App\Model\WeatherStation;
use Symfony\Contracts\EventDispatcher\Event;

/**
 * Class WeatherStateEvent
 */
class WeatherStateEvent extends Event
{
    public const NAME = 'weather.temperature';
    public const PRESSION = 'weather.pression';

    protected $weatherStation;

    /**
     * WeatherStateEvent constructor.
     *
     * @param WeatherStation $weatherStation
     */
    public function __construct(WeatherStation $weatherStation)
    {
        $this->weatherStation = $weatherStation;
    }

    /**
     * @return WeatherStation
     */
    public function getWeatherStation(): WeatherStation
    {
        return $this->weatherStation;
    }
}