<?php

namespace App\Model;

/**
 * Class Model*
 */
class WeatherStation
{
    /**
     * @var int
     */
    private $temperature;

    /**
     * @var int
     */
    private $pression = 200;

    /**
     * @return int
     */
    public function getTemperature(): int
    {
        return $this->temperature;
    }

    /**
     * @param int $temperature
     * @return WeatherStation
     */
    public function setTemperature(int $temperature): WeatherStation
    {
        $this->temperature = $temperature;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPression()
    {
        return $this->pression;
    }

    /**
     * @param mixed $pression
     * @return WeatherStation
     */
    public function setPression($pression)
    {
        $this->pression = $pression;
        return $this;
    }
}